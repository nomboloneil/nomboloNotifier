export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        oldUserPoolId
        name
        bio
        avatar {
          bucket
          region
          key
          uri
        }
        avatarThumbnail {
          bucket
          region
          key
          uri
        }
        avatarUpdatedAt
        userName
        pushKey
        aboutMe
        website
        email
        phone
        address
        age
        locale
        zoneinfo
        birthdate
        createdAt
        updatedAt
        posts {
          nextToken
        }
        asks {
          nextToken
        }
        comments {
          nextToken
        }
        fulfilledAsks {
          nextToken
        }
        notifications {
          title
          body
          origin
          remote
          hasCategory
          viewed
          visibility
          createdAt
          updatedAt
        }
        sentNotifications {
          nextToken
        }
        followers {
          nextToken
        }
        following {
          nextToken
        }
      }
      nextToken
    }
  }
`;