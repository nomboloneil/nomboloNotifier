import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput, ActivityIndicator, ScrollView, RefreshControl } from 'react-native';
import Amplify, { Auth, API, graphqlOperation } from 'aws-amplify';
import { listUsers } from './queries'
import awsmobile from './aws-exports';

export default function App() {
  Amplify.configure(awsmobile)
  const [users, setUsers] = useState(undefined)
  const [displayUsers, setDisplayUsers] = useState(undefined)
  const [pushKeys, setPushKeys] = useState(undefined)
  const [userNames, setUserNames] = useState(undefined)

  const [buttonReady, disableButton] = useState(false)
  const [title, setTitle] = useState(undefined)
  const [body, setBody] = useState(undefined)
  const [refreshing, setRefreshing] = React.useState(false);

 

  useEffect(() => {
    guestAccountSignIn()
  }, [])

  async function logKeys () {
    try {
      let result = await API.graphql(graphqlOperation(listUsers, { limit: 100000 }));
      let keys = await result.data.listUsers.items.map(a => a.pushKey).filter(a => a!= null)
      let userNames = await result.data.listUsers.items.map(a => a.id).filter(a => a!= null)
      setUsers(result.data.listUsers.items)
      setPushKeys(keys)
      setUserNames(userNames)
    } catch (err) {
      console.log(err)
    }
  }
  const guestAccountSignIn = () => {
    Auth.signIn('e.samuels-ellingson@nombolo.com', 'password')
    .then(() => {
      logKeys()
    })
    .catch((e) => {
      console.log('sign in error', e)
    })
  }

  const sendBatchNotification = () => {
    try {
      disableButton(false)
      if (pushKeys) {
        for (let key of keysNoNulls) {
          sendPushNotification(key)
        }
      }
    } catch (error) {
      console.log(error)
    } finally {
      disableButton(true)
    }
}
const logBatch = () => {
  try {
    if (title && body) {
      disableButton(true)
      if (pushKeys) {
        for (let key of pushKeys) {
          console.log(key)
        }
      }
    } else if (title) {
      alert("dont forget the body!!")
    } else if (body) {
      alert('how about a title??')
    } else {
      alert("no one likes empty notifications, how about adding a title and body?")
    }
  } catch (error) {
    alert(error)
  } 
}
 const tryButton = async () => {
  try {
    disableButton(true)
    console.log("hehe")
   await setTimeout(() => {
      console.log('timeout done')
    }, 10000000000000000)
  } catch (error) {
    console.log(error)
  } finally {
    disableButton(false)
  }
}
const sendPushNotification = async (key) => {
  const pushMessage = {
    to: key,
    sound: 'default',
    title: "Whats for lunch?",
    body: "Share your experience with nombolo",
    data: { data: 'goes here' },
    _displayInForeground: true,
  };
try  {
      const response = await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Accept-encoding': 'gzip, deflate',
          'Content-Type': 'application/json',
      },
    body: JSON.stringify(pushMessage),
  });
} catch (err) {
    console.log(err)
  } finally {
    console.log('done')
  }
};

const onRefresh = React.useCallback(() => {
  setRefreshing(true);
  logKeys()
  .then(() => setRefreshing(false))
  .then(() => disableButton(false))
  .then(() => alert('users refreshed, notify button enabled'));
}, []);
  return (
    <ScrollView
    refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
    style={styles.container}
    enabled={true}
    contentContainerStyle={styles.contentContainer}> 
    {!pushKeys && <ActivityIndicator size={"large"} style={{ flex: 1, position:"absolute", paddingTop: 100 }}
/>}
    <>
    { pushKeys && 
      <View style={{paddingTop: 100}}>
      <Text style={styles.text}>{pushKeys?.length} push tokens found for {userNames?.length} accounts</Text> 
   
      <TextInput
        style={styles.input}
        onChangeText={setTitle}
        value={title}
        placeholder="title"
      />
      <TextInput
        style={styles.input}
        onChangeText={setBody}
        value={body}
        placeholder="body"
      />
    <Button title={buttonReady ? `Message sent! 🚀`: `Send notification message to ${pushKeys?.length} users?`} disabled={buttonReady} onPress={logBatch} style={{paddingVertical: 20}}></Button>
    <View style={styles.space} /> 

    <Button title={displayUsers? "Hide Users and Tokens" : "Display Users and Tokens"} onPress={() => setDisplayUsers(!displayUsers)} style={{paddingVertical: 20}}></Button> 
    </View>
    }
    </>
    <View style={styles.space} /> 
    <View style={styles.space} /> 
      {users && users.map((user, index) => {
        return (
          <>
          {displayUsers &&
            <>
            <Text 
              style={{fontWeight: "bold", fontSize: 10}}
              key={index}> 
              {user.userName}: {user.pushKey}
            </Text>
            <View
              key={index}
              style={styles.space} /> 
            </>
          }
          </>
        )
      })}
      <StatusBar style="auto" />
      </ScrollView>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 100,
    width: 300,
    margin: 12,
    borderWidth: 1,
  },
  space: {
    width: 20, // or whatever size you need
    height: 20,
  },
  text: {
    fontWeight: 'bold',
    padding: 15
  }
});
